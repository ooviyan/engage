<?php

namespace App\Containers\Settings\Data\Seeders;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Seeders\Seeder;

class AuthorizeToAdminSeeder extends Seeder
{
    public function run()
    {

      // Assign to Admin
      $role = Apiato::call('Authorization@FindRoleTask', ['admin']);
      // Get All permission
      $allPermissions = Apiato::call('Authorization@GetAllPermissionsTask', [true]);
      $role->syncPermissions($allPermissionsNames = $allPermissions->pluck('name')->toArray());
      
    }
}
