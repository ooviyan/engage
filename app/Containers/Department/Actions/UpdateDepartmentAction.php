<?php

namespace App\Containers\Department\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateDepartmentAction extends Action
{
    public function run(Request $request)
    {   
        $fields=['name','code','description'];
        $data = array_filter($request->sanitizeInput($fields));
        $department = Apiato::call('Department@UpdateDepartmentTask', [$request->id, $data]);

        return $department;
    }
}
