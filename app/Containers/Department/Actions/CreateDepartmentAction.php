<?php

namespace App\Containers\Department\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateDepartmentAction extends Action
{
    public function run(Request $request)
    {
        $department = Apiato::call('Department@CreateDepartmentTask',[
        $request->name,
        $request->code,
        $request->description
        ]);

        return $department;
    }
}
