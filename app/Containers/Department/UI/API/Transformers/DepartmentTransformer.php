<?php

namespace App\Containers\Department\UI\API\Transformers;

use App\Containers\Department\Models\Department;
use App\Ship\Parents\Transformers\Transformer;

class DepartmentTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Department $entity
     *
     * @return array
     */
    public function transform(Department $entity)
    {
        $response = [
            'object' => 'Department',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'code' => $entity->code,
            'description' => $entity->description, 
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
