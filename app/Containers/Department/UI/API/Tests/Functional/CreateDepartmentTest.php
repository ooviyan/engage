<?php

namespace App\Containers\Department\UI\API\Tests\Functional;

use App\Containers\Department\Tests\ApiTestCase;

/**
 * Class CreateDepartmentTest.
 *
 * @group department
 * @group api
 */
class CreateDepartmentTest extends ApiTestCase
{

    // the endpoint to be called within this test (e.g., get@v1/users)
    protected $endpoint = 'post@v1/departments';

    // fake some access rights
    protected $access = [
        'permissions' => 'create-departments',
        'roles'       => '',
    ];

    /**
     * @test
     */
    public function testCreateDepartment_()
    {
        $data = [
            'name' => 'Department Name',
            'code' => 'DepartmentCode',
            'description' => 'Department Description',
        ];

        // send the HTTP request
        $response = $this->makeCall($data);

        // assert the response status
        $response->assertStatus(201);

        $this->assertResponseContainKeyValue([
            'name' => $data['name'],
            'code'  => $data['code'],
            'description'  => $data['description'],
        ]);
      
        $responseContent = $this->getResponseContentObject();
        $this->assertNotEmpty($responseContent->data);
    }
  
  public function testCreateDepartmentWithEmptyValues_()
    {
        $data = [
            'name' => '',
            'code' => '',
            'description' => '',
        ];

        // send the HTTP request
         $response = $this->makeCall($data);

        // assert response status is correct
        $response->assertStatus(422);

        // assert response contain the correct message
        $this->assertValidationErrorContain([
            'name' => 'The name field is required.',
            'code' => 'The code field is required.',
        ]);
    }
  
  public function testCreateDepartmentWithEmptyName_()
    {
        $data = [
            'name' => '',
            'code' => 'DepartmentCode',
            'description' => 'Department Description',
        ];

        // send the HTTP request
         $response = $this->makeCall($data);

        // assert response status is correct
        $response->assertStatus(422);

        // assert response contain the correct message
        $this->assertValidationErrorContain([
            'name' => 'The name field is required.',
        ]);
    }

    public function testCreateDepartmentWithEmptyCode_()
    {
        $data = [
            'name' => 'Department Code',
            'code' => '',
            'description' => 'Department Description',
        ];

        // send the HTTP request
         $response = $this->makeCall($data);

        // assert response status is correct
        $response->assertStatus(422);

        // assert response contain the correct message
        $this->assertValidationErrorContain([
            'code' => 'The code field is required.',
        ]);
    }

   public function testCreateDepartmentWithEmptyDescription_()
    {
        $data = [
            'name' => 'Department Code',
            'code' => 'DepartmentCode',
            'description' => '',
        ];

        // send the HTTP request
         $response = $this->makeCall($data);

        // assert response status is correct
        $response->assertStatus(201);

    }
  
    public function testCreateDepartmentWithDuplicateCode_()
    {
        $data = [
            'name' => 'Department Code',
            'code' => 'DepartmentCode',
            'description' => 'Department Description',
        ];

        // send the HTTP request
         $responseFirstCall = $this->makeCall($data);
         $responseSecondCall = $this->makeCall($data);
        // assert response status is correct
        $responseFirstCall->assertStatus(201);
        $responseSecondCall->assertStatus(422);
        // assert response contain the correct message
        $this->assertValidationErrorContain([
            'code' => 'The code has already been taken.',
        ]);
    }
  
}
