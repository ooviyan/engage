<?php

return [

    'table_names' => [

        'departments' => 'departments',
        'users' => 'users',
        'department_has_users' => 'department_has_users',

    ],

    'cache_expiration_time' => 60 * 24,

];
