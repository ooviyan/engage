<?php

namespace App\Containers\Department\Tasks;

use App\Containers\Department\Data\Repositories\DepartmentRepository;
use App\Containers\Department\Models\Department;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateDepartmentTask extends Task
{
    protected $repository;
  
    public function __construct(DepartmentRepository $repository)
    {
        $this->repository = $repository;
    }
  
    public function run(string $name, string $code, string $description = null):Department
    {
      
        try {
            $department = $this->repository->create([
                    'name' => $name,
                    'code' => $code,
                    'description' => $description,
                    ]);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
        return $department; 
    }
}