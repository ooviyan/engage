<?php

namespace App\Containers\Department\Data\Seeders;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Seeders\Seeder;


class DepartmentSeeder extends Seeder
{
    public function run()
    {
        Apiato::call('Department@CreateDepartmentTask', ['department 1', 'department1','department 1 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 2', 'department2','department 2 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 3', 'department3','department 3 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 4', 'department4','department 4 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 5', 'department5','department 5 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 6', 'department6','department 1 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 7', 'department7','department 2 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 8', 'department8','department 3 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 9', 'department9','department 4 description']);
        Apiato::call('Department@CreateDepartmentTask', ['department 10', 'department10','department 5 description']);
    }
}
