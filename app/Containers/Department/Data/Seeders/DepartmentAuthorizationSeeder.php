<?php

namespace App\Containers\Department\Data\Seeders;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Seeders\Seeder;

class DepartmentAuthorizationSeeder extends Seeder
{
    public function run()
    {
        // Default Permissions ----------------------------------------------------------
        $CrateDepartment = Apiato::call('Authorization@CreatePermissionTask', ['create-departments', 'Create a department.','Create Department']);
        $UpdateDepartment = Apiato::call('Authorization@CreatePermissionTask', ['update-departments', 'Update a department.','Update Department']);
        $DeleteDepartment = Apiato::call('Authorization@CreatePermissionTask', ['delete-departments', 'Delete a department.','Delete Department']);
        $SearchDepartment = Apiato::call('Authorization@CreatePermissionTask', ['search-departments', 'Find a department.','Search Department']);
        $ListDepartment = Apiato::call('Authorization@CreatePermissionTask', ['list-departments', 'Get All departments.','Get Department']);

        // Default Roles ----------------------------------------------------------------
        Apiato::call('Authorization@CreateRoleTask', ['department-admin', 'Department Administrator can Create, Update, Delete, Search and List.','Department Administrator'])->givePermissionTo($CrateDepartment,$UpdateDepartment,$DeleteDepartment,$SearchDepartment,$ListDepartment);
        Apiato::call('Authorization@CreateRoleTask', ['department-owner', 'Department Owner can Update, Search and List.', 'Department Owner'])->givePermissionTo($UpdateDepartment,$SearchDepartment,$ListDepartment);
        Apiato::call('Authorization@CreateRoleTask', ['department-member', 'Department Member can Search and List.', 'Department Member'])->givePermissionTo($SearchDepartment,$ListDepartment);      

        // Default Users (with their roles) ---------------------------------------------
        Apiato::call('User@CreateUserByCredentialsTask', [
            $isClient = false,
            'dept-admin@app.test',
            'password',
            'Department Administrator',
        ])->assignRole(Apiato::call('Authorization@FindRoleTask', ['department-admin']));

        Apiato::call('User@CreateUserByCredentialsTask', [
            $isClient = false,
            'dept-owner@app.test',
            'password',
            'Department Owner',
        ])->assignRole(Apiato::call('Authorization@FindRoleTask', ['department-owner']));
      
       Apiato::call('User@CreateUserByCredentialsTask', [
            $isClient = false,
            'dept-member@app.test',
            'password',
            'Department Member',
        ])->assignRole(Apiato::call('Authorization@FindRoleTask', ['department-member']));
      
    }
}