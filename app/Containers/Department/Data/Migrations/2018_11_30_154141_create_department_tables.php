<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentTables extends Migration
{
  
    public function up()
    {
        $tableNames = config('department.table_names');
        $foreignKeys = config('department.foreign_keys');
      
        Schema::create($tableNames['departments'], function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->string('description')->nullable();
            $table->timestamps();
        });
      
        Schema::create($tableNames['department_has_users'], function (Blueprint $table) use ($tableNames, $foreignKeys) {
            $table->integer('department_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('department_id')
                ->references('id')
                ->on($tableNames['departments'])
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on($tableNames['users'])
                ->onDelete('cascade');
          
            $table->primary(['department_id', 'user_id']);
        });      
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        $tableNames = config('department.table_names');

        Schema::drop($tableNames['department_has_users']);
        Schema::drop($tableNames['departments']);
    }
}